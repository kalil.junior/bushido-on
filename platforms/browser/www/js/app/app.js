var app = angular.module('pmcacho',['cp.ngConfirm']);


app.controller('menu.controller',['$scope','$http','$window','$ngConfirm','$interval', function ($scope,$http,$window,$ngConfirm,$interval) {

	$scope.login={};
	init();
	function init(){
		if($window.sessionStorage.getItem('login')!=null){			
			$scope.usuario=JSON.parse($window.sessionStorage.getItem('login'));
		}else{

			$window.location.href="index.html";	
		}
	}

	$scope.logout=function(){
			$ngConfirm({
				animation:'top',
				animationBounce: 3,
				title: 'Mensagem do Sistema',  
	            columnClass:'col-md-8 col-md-offset-2',          
	            content: '<div class="row">'+
		            	'<div class="col-sm-12 col-md-12">'+
		            			'Deseja sair do aplicativo?'+
		            	'</div>'+	            	
		            	'<div class="col-sm-12 col-md-12">'+
		            		'<hr/>'+
		            	'</div>'+
	            	'</div>',            	
	            theme:'light',
	            scope: $scope,
	            buttons:{
	                cancel: {
		                    text: '<i class="fa fa-times"></i>&nbsp;Cancelar',
		                    btnClass: 'btn-red',
		                    action: function(scope, button){
		                    	 

		                    }
	               		},
	               	ok: {
		                    text: '<i class="fa fa-check"></i>&nbsp;Sair',
		                    btnClass: 'btn-blue',
		                    action: function(scope, button){
		                    	$scope.usuario=null;
								$window.sessionStorage.removeItem('login');
								$window.location.href="index.html";

		                    }
	               		}
	               	
	               	
	               	}
	           	
			});	

		
	}

	$scope.exibir=false;

	$scope.exibirMenu=function() {
		

		if($("#menu-screen").hasClass("menu-hidden")){
			$("#menu-screen").removeClass("menu-hidden");
		}else{
			$("#menu-screen").addClass("menu-hidden")
		}
	}



	$scope.verificarDemandas=function () {
 		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.nao.lidas.php?matricula='+$scope.usuario.matricula).then(function(data) {
			var vcont=data.data;

			if(vcont.total>0){
				Push.create("Bushido On",{
					body:"Há "+vcont+" nova(s) demanda(s) para você!",
					icon:"img/logo2.png",
					timeout:4000,
					onClick:function () {
						window.focus();
						this.close();
					}					
	
				});
			}

		});
 	}


 }]);