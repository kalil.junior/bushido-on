app.controller('abrir.demanda.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {

	/*Declaração de variáveis*/	
	$scope.demanda={};
	
	$scope.demanda.solicitante={};
	$scope.secretarias=[];
	$scope.locais_secretaria=[];
	$scope.demanda.patrimonios=[];
	$scope.demanda.linhas=[];

	/*Área de chamada de funções*/
	carregar_secretarias();

	

	/*Declaração de funções*/

	function carregar_secretarias(){
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/pmcachoSismemo/consultarsecretarias').then(function(data){
				$scope.secretarias=data.data;
		});
	}


	/*Funções de Escopo*/

	$scope.consultar_servidor=function () {
		
		service.localizarServidor($scope.demanda.solicitante,$http);		
	};

	$scope.selecionarDepartamentos=function(){

		$http.get('http://sistemas.cachoeirinha.rs.gov.br/pmcachoSismemo/consultardepartamentos?sigla='+$scope.demanda.local_secretaria).then(function(data){
				$scope.locais_secretaria=data.data;
		});		
	};

	$scope.consultarPatrimonio=function(){
		var dat=$scope.patr;

			$http.get('http://sistemas.cachoeirinha.rs.gov.br/pmcachoServices/consultarcdbem?cdbem='+dat).then(
			function(data){
					$scope.patrimonio={};
					$scope.patrimonio.secretaria={};
					$scope.patrimonio.numeroPatrimonio=dat;
					if(data.data){
						if(data.data.dtbaixa==null){
							$scope.patrimonio.descricao=data.data.identificacao;			
							$scope.patrimonio.secretaria.predio=data.data.secretaria.codigoPredio;
							$scope.patrimonio.secretaria.unidade=data.data.secretaria.codigoUnidade
							$scope.patrimonio.secretaria.descricao=data.data.secretaria.unidade.descricaoUnidade;
							$scope.patr="";
							$scope.demanda.patrimonios.push($scope.patrimonio);
						}else{

							
							
							service.mensagem("O patrimônio informado está registrado como BAIXADO!",$scope,$ngConfirm);	
							$("nr_patrimonio").focus();
						}
					}else{
						service.mensagem("Informe um patrimônio válido!",$scope,$ngConfirm);
						$("nr_patrimonio").focus();
					}
					$scope.message="";
			}
	)};

    $scope.removerPatrimonio=function(x){
    	var ind=$scope.demanda.patrimonios.indexOf(x);		
		$scope.demanda.patrimonios.splice(ind,1);
    };

    $scope.consultarLinha=function(){
		var dat=$scope.nrlinha;
		
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.linhas.php?numero='+dat).then(function (data){				
			if(data.data!="false"){		
				$scope.linha=data.data;
				$scope.demanda.linhas.push($scope.linha);						
				$scope.nrlinha="";
			}else{
				service.mensagem("Informe uma linha válida!",$scope,$ngConfirm);
				$("nr_linha").focus();
			}
		});
		
				
	};	

	$scope.removerLinha=function(l){
		var ind=$scope.demanda.linhas.indexOf(l);		
		$scope.demanda.linhas.splice(ind,1);	
	};


	$scope.registrar_demanda=function () {

		$scope.demanda.usuario=JSON.parse($window.sessionStorage.getItem('login'));		

		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/criar.demanda.php',$scope.demanda).then(function(data){
			$scope.demanda=data.data;
			service.confirmacao("Sua demanda "+ $scope.demanda.id+" foi criada!","index2.html",$scope,$ngConfirm,$window);
		});
		
	}


}]);