app.controller('acesso.controller',['$scope','$http','$window','$ngConfirm','service', function ($scope,$http,$window,$ngConfirm,service) {

	$scope.usuario={};

	$scope.acessar=function () {

		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/login.php',$scope.usuario).then(function(data){
			var token=data.data;
			if(token.codigo<=0){
				$window.sessionStorage.setItem('login',JSON.stringify(token.obj));

				$window.location.replace('index2.html');
			}else{
				service.mensagem('Usuário ou senha incorretos!',$scope,$ngConfirm);
			}
						
		});
	}

}]);