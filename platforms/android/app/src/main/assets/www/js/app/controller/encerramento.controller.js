app.controller('encerramento.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {
	$scope.follow={};
 	init();
 	function init() {
 		var url = window.location.href;
		var arg = url.split('?')[1].split('=')[1];
		$scope.follow.demanda=arg;
		$scope.follow.tipo=0;
		$scope.follow.usuario=JSON.parse($window.sessionStorage.getItem('login'));		
	}


	$scope.encerrar=function () {
			$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/encerrar.demanda.php',$scope.follow).then(function(data){ 			
 			service.confirmacao('Demanda encerrada com sucesso!','demanda.html?demanda='+$scope.follow.demanda,$scope,$ngConfirm,$window); 			
 		});
	}


}]);