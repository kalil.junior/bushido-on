 app.controller('followup.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {

 	$scope.lista=[];
 	$scope.follow={};
 	init();
 	function init() {
 		var url = window.location.href;
		var arg = url.split('?')[1].split('=')[1];
		$scope.follow.demanda=arg;
		$scope.follow.tipo=0;
		$scope.follow.usuario=JSON.parse($window.sessionStorage.getItem('login'));
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.follow.ups.php?demanda='+arg).then(function(data) {
			$scope.lista=data.data;
		});	

 	}

 	$scope.verificarDemandas=function () {
 		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.nao.lidas.php?matricula='+usuario.matricula).then(function(data) {
			var vcont=data.data;

			if(vcont.total>0){
				Push.create("Bushido On",{
					body:"Há "+vcont+" nova(s) demanda(s) para você!",
					icon:"img/logo2.png",
					timeout:4000,
					onClick:function () {
						window.focus();
						this.close();
					}					
	
				});
			}

		});
 	}



 	$scope.enviarFollow=function () {
 		
 		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/incluir.follow.up.php',$scope.follow).then(function (data) {
 			service.mensagem('Follow Up incluído!',$scope,$ngConfirm);
			$scope.follow.descricao="";
			$scope.follow.privado=false;
			$scope.follow.resposta=false;
			$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.follow.ups.php?demanda='+$scope.follow.demanda).then(function(data) {
				$scope.lista=data.data;
			});	 			
 		});
 	}

 }]);