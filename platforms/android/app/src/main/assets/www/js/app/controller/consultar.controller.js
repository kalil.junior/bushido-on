app.controller('consultar.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {
	$scope.$sce=$sce;
	$scope.lista=[];
	$scope.grupos=[];
	$scope.filtros={};
	init();

	function init(){
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.grupos.php').then(function(data) {
			$scope.grupos=data.data;
		});
	}

	$scope.filtro={};
	$scope.ativar_pesquisa_avacada=function () {

		$(".filtros-avancados").addClass("show-filtros");
	}


	$scope.aplicar_pesquisa_avacada=function () {
		$(".loader").removeClass("hide-loader");		
		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.demandas.php',$scope.filtros).then(function (data) {			
			$(".filtros-avancados").removeClass("show-filtros");	
			$scope.lista=data.data;			
			$(".loader").addClass("hide-loader");
		});		
	}

	$scope.ativar_pesquisa=function () {
		$(".loader").removeClass("hide-loader");		
		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.demandas.php',$scope.filtros).then(function (data) {						
			$scope.lista=data.data;			
			$(".loader").addClass("hide-loader");
		});		
	}



	$scope.limpar_pesquisa=function () {
		$scope.filtros={};		
	}

}]);