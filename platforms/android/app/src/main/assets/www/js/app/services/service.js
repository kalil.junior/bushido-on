app.service('service',function(){	
	return {
		mensagem: function (mensagem,scope,ngConfirm){
			scope.mensagem=mensagem;
			ngConfirm({
				animation:'top',
				animationBounce: 3,
				title: 'Mensagem do Sistema',  
	            columnClass:'col-md-8 col-md-offset-2',          
	            content: '<div class="row">'+
		            	'<div class="col-sm-12 col-md-12">'+
		            			'{{mensagem}}'+
		            	'</div>'+	            	
		            	'<div class="col-sm-12 col-md-12">'+
		            		'<hr/>'+
		            	'</div>'+
	            	'</div>',            	
	            theme:'dark',
	            scope: scope,
	            buttons:{
	                ok: {
		                    text: '<i class="fa fa-check"></i>&nbsp;OK',
		                    btnClass: 'btn-blue',
		                    action: function(scope, button){
		                    	
		                    }
	               		}
	               	
	               	}
	           	
			});
		},
		diasNoMes:function (ano,mes){
    		/*COMO USAR A FUNÇÃO:
   		 	1 - A função retorna sempre os dias do mês anterior ao solicitado. Para tanto, informa-se o ano corrente do cálculo, o mês + 1 e o dia sempre em 0*/    
        	return new Date(ano, mes, 0).getDate();
    	},
    	confirmacao:function (mensagem,url,scope,ngConfirm,window){
	        scope.mensagem=mensagem;
	        ngConfirm({
	            title: 'Mensagem do Sistema',  
	            columnClass:'col-md-8 col-md-offset-2',          
	            content: '<div class="row">'+
	                    '<div class="col-sm-12 col-md-12">'+
	                            '{{mensagem}}'+
	                    '</div>'+                   
	                '</div>',               
	            theme:'dark',
	            scope: scope,
	            buttons:{
	                ok: {
	                        text: '<i class="fa fa-check"></i>&nbsp;OK',
	                        btnClass: 'btn-blue',
	                        action: function(scope, button){	                        	
	                            window.location.href=url;
	                        }
	                    }
	                
	                }
	            
	        });
    	},    	
		gerarRelatorio:function (url,nome,http){				
			
			http.get(url,{responseType:'arraybuffer'}).then(function (data) {

			    var file = window.URL.createObjectURL(new Blob([data.data]));
			    var a = document.createElement("a");
		        a.href = file;
		        a.download =  nome;
		        document.body.appendChild(a);
		        a.click();
		        
		        window.onfocus = function () {                     
		          document.body.removeChild(a)
		        }	 
			});					

		},
		makeId:function () {
		  var text = "";
		  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		  for (var i = 0; i < 5; i++){
		    text += possible.charAt(Math.floor(Math.random() * possible.length));
		  }

		  return text;
		},
		consultar:function(url,http,scope){			
			scope.pagina_corrente=1;
			scope.numero_paginas=0;
			if(!scope.registros_pag){
				scope.registros_pag=10;
			}
			scope.total_paginas=[];
			http.get(url).then(function(data){
					
				scope.lista=data.data;

				// scope.lista = orderBy(scope.lista,'-id', 'true');

				var total=scope.lista.length;
			   
		    	 while(total>scope.registros_pag){
		        	scope.numero_paginas=scope.numero_paginas+1;
		        	total=total-scope.registros_pag;        
		        	var obj={num:angular.copy(scope.numero_paginas)};
		        	scope.total_paginas.push(obj);
		      	};
		      	if(total>0){
			    	scope.numero_paginas=scope.numero_paginas+1;
			    };

			});

		},
		localizarServidor:function(obj,http){
			
			var usuario_oracle={};			
		
			http.get('http://sistemas.cachoeirinha.rs.gov.br/pmcachoServices/consultarmatricula?matricula='+obj.matricula).then( 						
				function (data){
					usuario_oracle=data.data;				
					obj.nome=usuario_oracle.nome;			
					obj.cpf=usuario_oracle.cpf;	
					obj.secretaria=usuario_oracle.secretaria.nome;

					if(obj.cpf.length<14){
						while(obj.cpf.length<11){
							obj.cpf='0'+obj.cpf;
						};

						obj.cpf=obj.cpf.substring(0,3)+'.'+obj.cpf.substring(3,6)+'.'+obj.cpf.substring(6,9)+'-'+obj.cpf.substring(9,11);						
						
					};


					http.post('http://sistemas.cachoeirinha.rs.gov.br/pmcachoLogin/buscaMatricula',obj.matricula).then( function(data){

						if(data.data){
							obj.cadastrado=true;
							$('input[type="email"]').focus();
						}

					}); 

				}
			);

			
		},
		validarSenhas:function(s1,s2,ngConfirm){
			var valido=false;
			if(s1 != s2){
				ngConfirm({
				    title: 'Registro de Usuários',
				    content: 'As senhas NÂO conferem!',
				    buttons: {
				       ok: {
				        	text: '<i class="fa fa-paper-check"></i>&nbsp;Ok',
                        	btnClass: 'btn-blue',
				        	action:function () {
				        	 valido= false;
				        	}	
				        }				        
				    },
				    theme:'bootstrap'
				});
			}else{
				valido= true;
			};

			return valido;
		},
		validarEmail:function(email,ngConfirm){
			var valido=false;
			if(email){

				if(!email.split("@")[1]!="cachoeirinha.rs.gov.br"){
					ngConfirm({
					    title: 'Registro de Usuários',
					    content: 'E-mail inválido!',
					    buttons: {
					       ok: {
					        	text: '<i class="fa fa-paper-check"></i>&nbsp;Ok',
	                        	btnClass: 'btn-blue',
					        	action:function () {
					        	 valido= false;
					        	}	
					        }				        
					    },
					    theme:'bootstrap'
					});
				}else{
					 valido= true;
				}
			}else{
					ngConfirm({
					    title: 'Registro de Usuários',
					    content: 'Informe o e-mail!',
					    buttons: {
					        ok: {
					        	text: '<i class="fa fa-paper-check"></i>&nbsp;Ok',
	                        	btnClass: 'btn-blue',
					        	action:function () {
					        	 valido= false;
					        	}	
					        }			        
					    },
					    theme:'bootstrap'
					});
			}

			return valido;
		},
		recalcularPaginas:function(scope){
			scope.pagina_corrente=1;
			scope.total_paginas=[];
			var total=scope.lista.length;
			   
	    	while(total>scope.registros_pag){
	        	scope.numero_paginas=scope.numero_paginas+1;
	        	total=total-scope.registros_pag;        
	        	var obj=scope.numero_paginas;
	        	scope.total_paginas.push(obj);
	      	};
	      	if(total>0){
		    	scope.numero_paginas=scope.numero_paginas+1;
		    };			
		},
		getUser:function($window){
			return JSON.parse($window.sessionStorage.getItem('login'));
		},
		paginaAnterior:function(scope){
			if(scope.pagina_corrente>(1)){
				scope.pagina_corrente=scope.pagina_corrente-1;
			}else{
				scope.pagina_corrente=1;
			}	
		},
		proximaPagina:function(scope){
			var total=scope.lista.length;		

			if(scope.pagina_corrente<(total+1)){
				scope.pagina_corrente=scope.pagina_corrente+1;
			}else{
				scope.pagina_corrente=total-1;
			}
		},
		setPage:function(indice,scope){
			var total=scope.lista.length;
		
			scope.pagina_corrente=indice*scope.registros_pag;
		},
		formatDate:function(dat){
			var date = new Date(dat);
            var userTimezoneOffset = date.getTimezoneOffset() * 60000;
            return new Date(date.getTime() + userTimezoneOffset);
		},
		to_Date_String:function(dat){	
			var ano=dat.substring(0,4);
			var mes=dat.substring(3,2);
			var dia=dat.substring(5,2);

			var dat=dia+'/'+mes+'/'+ano;

			return dat;
		},
		consultarCEP:function($http,obj){
			$http({
				method:'GET',
				url:'http://viacep.com.br/ws/'+obj.cep + '/json/' 
			}).then(
			function(data){
				var endereco=data.data;
				obj.logradouro=endereco.logradouro;
				obj.bairro=endereco.bairro;
				obj.complemento=endereco.complemento;
				obj.cidade=endereco.localidade;
				obj.uf=endereco.uf;
							

				$("#numero").focus();
			},function(error){obj.msg_err=error;});
		},
		mes_por_extenso:function(month){
			var mes="";
			switch(month){
				case 0:mes="Janeiro";break;
				case 1:mes="Fevereiro";break;
				case 2:mes="Março";break;
				case 3:mes="Abril";break;
				case 4:mes="Maio";break;
				case 5:mes="Junho";break;
				case 6:mes="Julho";break;
				case 7:mes="Agosto";break;
				case 8:mes="Setembro";break;
				case 9:mes="Outubro";break;
				case 10:mes="Novembro";break;
				case 11:mes="Dezembro";break;
			}
			return mes;
		},
		to_Date:function(dat){	
						
			var ano=dat.getFullYear();
			var mes=(dat.getMonth()+1);
			var dia=dat.getDate();

			var dat=ano+'/'+mes+'/'+dia;


			return dat;
		}
       

         

	};
});