app.controller('inicio.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {

	$scope.$sce=$sce;

	$scope.lista=[];

	function init(){

		var usuario=JSON.parse($window.sessionStorage.getItem('login'));

		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.demandas.php?matricula='+usuario.matricula).then(function(data) {
			$scope.lista=data.data;

			var lx = angular.copy($scope.lista);
			var vcont=0;
			for(var l=0;l<lx.length;l++){
				if(lx[l].lido=="f"){
					vcont+=1;
				}				
				
			};

			if(vcont>0){
				Push.create("Bushido On",{
					body:"Há "+vcont+" nova(s) demanda(s) para você!",
					icon:"img/logo2.png",
					timeout:4000,
					onClick:function () {
						window.focus();
						this.close();
					}
	
				});
			}



		});
	}

	init();
	

}]);