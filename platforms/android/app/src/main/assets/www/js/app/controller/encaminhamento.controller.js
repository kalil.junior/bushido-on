app.controller('encaminhamento.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {
	$scope.lista=[];
 	$scope.grupo={};
 	init();
 	function init() {
 		var url = window.location.href;
		var arg = url.split('?')[1].split('=')[1];		
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.grupos.php').then(function(data) {
			$scope.lista=data.data;

			$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.grupo.demanda.php?demanda='+arg).then(function(data) {
				$scope.grupo.id=data.data.grupo;
				$scope.grupo.demanda=arg;
				$scope.grupo.usuario=JSON.parse($window.sessionStorage.getItem('login'));

			});
		});	

 	}




 	$scope.encaminhar=function () {
 		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/encaminhar.demanda.php',$scope.grupo).then(function(data){
 			
 			service.confirmacao('Demanda encaminhada com sucesso!','demanda.html?demanda='+$scope.grupo.demanda,$scope,$ngConfirm,$window); 			
 		});
 	}

}]);