app.controller('situacao.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {
	$scope.demanda={};

	init();

 	function init() {
 		var url = window.location.href;
		var arg = url.split('?')[1].split('=')[1];		
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.situacao.demanda.php?demanda='+arg).then(function(data) {
			$scope.demanda.id=arg;
			$scope.demanda.situacao=data.data.situacao;
			$scope.demanda.usuario=JSON.parse($window.sessionStorage.getItem('login'));
		});

 	}


	

	$scope.atualizar=function () {
		
 		$http.post('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/definir.situacao.php',$scope.demanda).then(function(data){ 	
 			
 			service.confirmacao('Situação da demanda atualizada com sucesso!','demanda.html?demanda='+$scope.demanda.id,$scope,$ngConfirm,$window); 			
 		});
 	}

}]);