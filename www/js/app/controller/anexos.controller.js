app.controller('anexos.controller',['$scope','$http','$window','$ngConfirm','service','$sce', function ($scope,$http,$window,$ngConfirm,service,$sce) {

	$scope.lista=[];

	init();

 	function init() {
 		var url = window.location.href;
		var arg = url.split('?')[1].split('=')[1];		
		$http.get('http://sistemas.cachoeirinha.rs.gov.br/bushido/php/consultar.anexos.php?demanda='+arg).then(function(data) {
			$scope.lista=data.data;
		});	

 	};

	$scope.baixarArquivo=function(f){
		downloadFile(f);
		
	};
	
	function downloadFile(f){
		var sampleBytes = base64ToArrayBuffer(f.arquivo);
		saveByteArray([sampleBytes], f.nome);
	};

	function base64ToArrayBuffer(base64) {
	    var binaryString =  window.atob(base64);
	    var binaryLen = binaryString.length;
	    var bytes = new Uint8Array(binaryLen);
	    for (var i = 0; i < binaryLen; i++)        {
	        var ascii = binaryString.charCodeAt(i);
	        bytes[i] = ascii;
	    }
	    return bytes;
	};

	var saveByteArray = (function () {
	    var a = document.createElement("a");
	    document.body.appendChild(a);
	    a.style = "display: none";
	    return function (data, name) {
	        var blob = new Blob(data, {type: "octet/stream"}),
	            url = window.URL.createObjectURL(blob);
	        a.href = url;
	        a.download = name;
	        a.click();	        
	        window.URL.revokeObjectURL(url);
	    };
	}());


}]);